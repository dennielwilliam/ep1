#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include <iostream>
#include <vector>
#include "produto.hpp"
#include "estoque.hpp"

class Carrinho : public Estoque
{
	public:
	Carrinho();
	~Carrinho();
	
	double calcularTotal();

};

#endif