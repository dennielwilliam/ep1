#ifndef MODOESTOQUE_HPP
#define MODOESTOQUE_HPP


void modoEstoque();
void cadastrarProduto(Estoque *);
void cadastrarCategoria();
void atualizarProduto(Estoque *);
void atualizarLista(vector<string> *);
void imprimirEstoque(Estoque *);
void atualizarEstoque(Estoque *);



#endif