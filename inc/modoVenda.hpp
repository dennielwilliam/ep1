#ifndef MODOVENDA_HPP
#define MODOVENDA_HPP

#include "cliente.hpp"
#include "socio.hpp"
#include "carrinho.hpp"
#include "estoque.hpp"
#include "produto.hpp"
#include "modoVenda.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

void modoVenda();
Cliente *encontrarCadastro();
bool encontrarCliente(Cliente *);
bool encontrarSocio(Cliente *);

#endif