#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP

#include <iostream>
#include <vector>
#include "produto.hpp"

using namespace std;

class Estoque
{
private:
	vector<Produto *> produto;
	vector<int> quantidade;

public:

	Estoque();

	~Estoque();

	void setProduto(Produto *produto);
	Produto *getProduto(int i);

	unsigned getSizeProduto();

	void setQuantidade(int qtd);
	void setQuantidade(int i,int qtd);
	
	int getQuantidade(int i);


};

#endif