#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <iostream>
#include <string>
#include "carrinho.hpp"

using namespace std;

class Cliente
{	

private:

	string nome;
	string email;
	string numeroCel;
	string cpf;
	vector<string> categoria;

public:

	Cliente(); 
	~Cliente();

	void setNome(string nome);
	string getNome();
	void setEmail(string email);
	string getEmail();
	void setNumeroCel(string numeroCel);
	string getNumeroCel();
	void setCPF(string cpf);
	string getCPF();
	void setCategoria(string categoria);
	int getSizeCategoria();
	string getCategoria(int i);

	virtual void cadastrar();
	void atualizarDados(string nome, string email, string numeroCel, string cpf,int numeroCat,vector <string> categoria);
	virtual double pagamentoTotal(Carrinho *carrinho);
	
};

#endif