#ifndef SOCIO_HPP
#define SOCIO_HPP

#include <iostream>
#include <string>
#include "cliente.hpp"
#include "carrinho.hpp"

class Socio : public Cliente
{
	public:
		Socio();
		~Socio();
		double pagamentoTotal(Carrinho * carrinho);
		void cadastrar();	
};

#endif