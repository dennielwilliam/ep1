#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Produto
{
	//Atributos
private:
	string nome;
	double valor;
	vector<string> categoria;

public:
	//Construtores
	Produto();

	//Destrutores
	~Produto();

	//Métodos
	void setNome(string nome);
	string getNome();

	void setValor(double valor);
	double getValor();

	void setCategoria(string categoria);
	string getCategoria(int i);
	unsigned getSizeCategoria();

};

#endif