#include "cliente.hpp"
#include "carrinho.hpp"
#include "estoque.hpp"
#include "produto.hpp"
#include "modoEstoque.hpp"
#include "aux.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

void modoEstoque()
{
	Estoque estoque;
	int resp = 1;

	atualizarEstoque(&estoque);

	while(1){
		ClearScreen();
		cout << "Modo de Estoque\n\nEscolha um das opcoes abaixo:" << endl;
		cout << "1. Cadastrar novo produto" << endl;
		cout << "2. Cadastrar nova categoria" << endl;
		cout << "3. Atualizar quantidade de um produto" << endl;
		cout << "0. Sair" << endl;

		cin >> resp;

		if(resp == 1)
			cadastrarProduto(&estoque);
		else if(resp == 2)
			cadastrarCategoria();
		else if(resp == 3)
			atualizarProduto(&estoque);
		else if(resp == 0)
			break;
		else
			cout << "Escolha invalida. Escolha novamente" << endl;

	}

	ofstream productFile("produto.txt", ios::out);
	
	if(!productFile) 
	{
		cerr << "Nao foi possivel abrir no arquivo produto.txt" << endl;
		exit(1);
	}

	for(int i = 0;i < estoque.getSizeProduto(); i++){
		productFile << estoque.getProduto(i)->getNome();
		productFile.put('\n');
		productFile << estoque.getProduto(i)->getValor();
		productFile.put('\n');
		productFile << estoque.getProduto(i)->getSizeCategoria();
		productFile.put('\n');
		for(int j = 0;j < estoque.getProduto(i)->getSizeCategoria(); j++){
			productFile << estoque.getProduto(i)->getCategoria(j);
			productFile.put('\n');
		}
	}

	productFile.close();

	ofstream estoqueFile("estoque.txt", ios::out);

	if(!estoqueFile){
		cerr << "Nao foi possivel abrir o arquivo estoque.txt" << endl;
		exit(4);
	}

	for(int i = 0;i < estoque.getSizeProduto(); i++){
		estoqueFile << estoque.getProduto(i)->getNome();
		estoqueFile.put('\n');
		estoqueFile << estoque.getProduto(i)->getValor();
		estoqueFile.put('\n');
		estoqueFile << estoque.getQuantidade(i);
		estoqueFile.put('\n');
	}

	estoqueFile.close();
}

void cadastrarProduto(Estoque *estoque)
{
	vector<string> categoria;
	vector<string> categoriaAux;
	Produto *produto = new Produto();
	string nomeProduto;
	int qtd;
	double preco;
	ifstream categoriaFile("categoria.txt", ios::in);
	ClearScreen();
	getchar();
	cout << "Digite o nome do produto: " << endl;
	getline(cin,nomeProduto);

	if(!categoriaFile)
	{	
		int resp; 
		while(1)
		{	
			ClearScreen();
			cout << "Nao foi encontrado categorias. Deseja criar uma categoria?\n1.Sim\t2.Nao" << endl;
			cin >> resp;

			if(resp == 1){
				categoriaFile.close();
				cadastrarCategoria();
				ifstream categoriaFile("categoria.txt", ios::in);
				cout << "Pressione enter para continuar...";
				break;
			}
			else if(resp == 2)
				return;
			else
				cout << "Opcao invalida!" << endl;
		}
	}

	while(1){

		int resp;

		ClearScreen();
		atualizarLista(&categoria);
		cout << "Escolha a categoria do produto:\n(caso deseje adicionar uma nova categoria digite " << categoria.size() << ")" << endl;

		for(int i=0;i < categoria.size();i++)
		{
			cout << i << "." << categoria[i] << endl;
		}

		cin >> resp;

		if(resp < categoria.size()){
			categoriaAux.push_back(categoria[resp]);
			cout << "Deseja adicionar mais alguma categoria ao produto? \n1.Sim\t0.Nao\n" << endl;
			int resp1;
			cin >> resp1;
			if(resp1 == 0)
				break;
		}
		else if(resp == categoria.size()){
			categoriaFile.close();
			cadastrarCategoria();
			ifstream categoriaFile("categoria.txt", ios::in);
			cout << "Pressione enter para continuar...";
		}
		else
			cout << "Opcao invalida. Digite novamente!";	
	}

	cout << "Digite a quantidade: ";
	cin >> qtd;
	cout << "Digite o preco: ";
	cin >> preco;


	produto->setNome(nomeProduto);
	produto->setValor(preco);

	for(int i = 0;i < categoriaAux.size();i++)
	{
		produto->setCategoria(categoriaAux[i]);
	}

	estoque->setProduto(produto);
	estoque->setQuantidade(qtd);

	categoriaFile.close();
}

void cadastrarCategoria()
{
	string categoria;
	ofstream categoriaFile("categoria.txt", ios::app);
	
	if (!categoriaFile) 
	{
		cerr << "Nao foi possivel gravar no arquivo" << endl;
		exit(1);
	}
	ClearScreen();
	getchar();
	cout << "Digite a categoria que deseja adicionar: " << endl;
	getline(cin,categoria);
	categoriaFile << categoria;
	categoriaFile.put('\n');

	categoriaFile.close();
}

void atualizarProduto(Estoque *estoque)
{
	int resp;
	int qtd;

	ClearScreen();
	if(estoque->getSizeProduto() == 0){
		cout << "Nao existe produtos cadastrados" << endl;
		return;
	}

	imprimirEstoque(estoque);

	cin >> resp;
	if(resp == estoque->getSizeProduto())
		return;

	cout << "Digite a quantidade atualizada do produto: " << endl;
	cin >> qtd;

	estoque->setQuantidade(resp, qtd);
}

void atualizarLista(vector<string> *categoria)
{
	ifstream categoriaFile("categoria.txt", ios::in);

	string categoriaAux;

	if(categoria->size() > 0)
		categoria->erase(categoria->begin(),categoria->begin() + categoria->size());

	while(getline(categoriaFile,categoriaAux)){
		categoria->push_back(categoriaAux);
	}

	categoriaFile.close();
}

void imprimirEstoque(Estoque *estoque)
{
	Produto *produto;

	cout << "Escolha qual produto deseja atualizar: (digite " << estoque->getSizeProduto() << " para finalizar)" << endl;


	cout << "[ Nome - Quantidade]" << endl;
		
	for(int i = 0;i < estoque->getSizeProduto();i++)
	{	
		produto = estoque->getProduto(i);
		cout << i << "." << produto->getNome() << " - " << estoque->getQuantidade(i) << endl; 
	}
}

void atualizarEstoque(Estoque *estoque)
{
	Produto *produto;
	string categoria;
	string nome;
	double valor;
	int size;
	int qtd;

	ifstream productFile("produto.txt", ios::in);

	while(getline(productFile,nome)){
		produto = new Produto();
		produto->setNome(nome);
		productFile >> valor;
		produto->setValor(valor);	
		productFile >> size;
		getline(productFile,categoria);
		for(int i = 0;i < size;i++)
		{
			getline(productFile,categoria);
			produto->setCategoria(categoria);
		}
		estoque->setProduto(produto);
	}

	ifstream estoqueFile("estoque.txt", ios::in);

	while(getline(estoqueFile,nome))
	{
		estoqueFile >> valor;
		estoqueFile >> qtd;
		estoque->setQuantidade(qtd);
	}

	productFile.close();
}