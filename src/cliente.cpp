#include "cliente.hpp"
#include <iostream>
#include <fstream>

using namespace std;

Cliente::Cliente()
{	
	setNome(" ");
	setEmail(" ");
	setNumeroCel(" ");
	setCPF(" ");
}

Cliente::~Cliente()
{	
	
}

void Cliente::setNome(string nome)
{
	this->nome = nome;
}

string Cliente::getNome()
{
	return nome;
}

void Cliente::setEmail(string email)
{
	this->email = email;
}

string Cliente::getEmail()
{
	return email;
}

void Cliente::setNumeroCel(string numeroCel)
{
	this->numeroCel = numeroCel;
}

void Cliente::setCPF(string cpf)
{
	this->cpf = cpf;
}

string Cliente::getCPF()
{
	return cpf;
}

string Cliente::getNumeroCel()
{
	return numeroCel;
}

void Cliente::setCategoria(string categoria)
{
	this->categoria.push_back(categoria);
}

string Cliente::getCategoria(int i)
{
	return categoria[i];
}

int Cliente::getSizeCategoria()
{
	return categoria.size();
}

double Cliente::pagamentoTotal(Carrinho *carrinho)
{	
	return carrinho->calcularTotal(); 
}

void Cliente::atualizarDados(string nome, string email, string numeroCel,string cpf,int numeroCat,vector<string> categoria)
{
	setNome(nome);
	setEmail(email);
	setNumeroCel(numeroCel);
	setCPF(cpf); 

	for(int i = 0;i < numeroCat;i++){
		setCategoria(categoria[i]);
	}
}

void Cliente::cadastrar()
{	

	ofstream cadastroFile("clientes.txt", ios::app);
	
	if (!cadastroFile) 
	{
		cerr << "Nao foi possivel gravar no arquivo" << endl;
		exit(1);
	}

	cadastroFile << getNome();
	cadastroFile.put('\n');

	cadastroFile << getEmail();
	cadastroFile.put('\n');

	cadastroFile << getNumeroCel();
	cadastroFile.put('\n');

	cadastroFile << getCPF();
	cadastroFile.put('\n');

	cadastroFile << getSizeCategoria();
	cadastroFile.put('\n');

	for(int i = 0;i < getSizeCategoria();i++){
		cadastroFile << getCategoria(i);
		cadastroFile.put('\n');
	}

	cadastroFile.close();

} 

