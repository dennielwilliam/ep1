#include "cliente.hpp"
#include "carrinho.hpp"
#include "estoque.hpp"
#include "produto.hpp"
#include "modoVenda.hpp"
#include "modoEstoque.hpp"
#include "aux.hpp"
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

void modoVenda()
{	
	Cliente *cliente;
	Estoque estoque;
	Carrinho carrinho;

	atualizarEstoque(&estoque);

	cliente = encontrarCadastro();

	if(cliente != NULL)
		while(1)
		{	int resp;
			ClearScreen();
			cout << "Selecione o modo de operacao: " << endl;
			cout << "1. Adicionar Produto ao Carrinho" << endl;
			cout << "2. Visualizar Carrinho" << endl;
			cout << "3. Finalizar Compra" << endl;
			cout << "4. Cancelar Compra" << endl;
			cin >> resp;
			getchar();

			if(resp == 1){
				
				unsigned qtd;

				ClearScreen();

				cout << "Lista de produtos: (escolha o produto que deseja adicionar)\n[Nome ---- Categoria ---- Preco]\n" << endl;
				for(int i = 0;i < (int) estoque.getSizeProduto();i++){
					cout << i << "." << (estoque.getProduto(i))->getNome() << " ---- " << (estoque.getProduto(i))->getCategoria(0);
					cout << " ---- R$ " << setprecision(2)  << fixed << (estoque.getProduto(i))->getValor() << endl;
				}

				cin >> resp;
				cout << "Insira quantidade que deseja comprar: ";
				cin >> qtd;

				if((int) qtd > estoque.getQuantidade(resp))
				{
					cout << "Nao tem produtos disponiveis no estoque" << endl;
				}
				else{
					carrinho.setProduto(estoque.getProduto(resp));
					carrinho.setQuantidade(qtd);
					estoque.setQuantidade(estoque.getQuantidade(resp) - qtd);	
				}
			}
			else if(resp == 2){
				cout << "Carrinho:\n[Nome ---- Quantidade ---- Preco]\n" << endl;
				for(int i = 0;i < (int) carrinho.getSizeProduto();i++)
				{
					cout << (carrinho.getProduto(i))->getNome() << " ---- " << carrinho.getQuantidade(i);
					cout << " ---- R$ " << setprecision(2)  << fixed << (carrinho.getProduto(i))->getValor() << endl;
				}
				cout << "\n------------------------------------------|" << endl;
				cout << "Total: R$ " << carrinho.calcularTotal() << endl; 
				cout << "\n\nPressione enter para continuar...";
				getchar();
			}
			else if(resp == 3){
				cout << "Pagamento Total: R$ " << setprecision(2) << fixed << cliente->pagamentoTotal(&carrinho) << endl;
				cout << "Pressione enter para continuar...";
				getchar();
				break;
			}
			else if(resp == 4){
				break;
			}
			else{
				cout << "Opcao invalida. Escolha novamente" << endl;
			}
		}

	delete cliente;	
}

Cliente * encontrarCadastro()
{
	
	Cliente clienteAux;
	string nome;
	string email;
	string numero;
	string cpf;
	vector<string> categoria;
	vector<string> categoriaAux;
	bool resultCliente;
	bool resultSocio;

	getchar();
	cout << "Digite o nome completo: " << endl;
	getline(cin , nome);
	clienteAux.setNome(nome);

	resultCliente = encontrarCliente(&clienteAux);

	if(resultCliente == false)
		resultSocio = encontrarSocio(&clienteAux);

	nome = clienteAux.getNome();
	email = clienteAux.getEmail();
	numero = clienteAux.getNumeroCel();
	cpf = clienteAux.getCPF();

	for(int i = 0;i < clienteAux.getSizeCategoria(); i++)
		categoria.push_back(clienteAux.getCategoria(i));

	if(resultCliente == true ||	resultSocio == true){
		ClearScreen();
		cout << "Cadastro encontrado!\nPressione enter para continuar..." << endl;
		getchar();

		if(resultCliente == true){
			Cliente *cliente = new Cliente();
			cliente->atualizarDados(nome,email,numero,cpf,categoria.size(),categoria);
			return cliente;
		}

		else if(resultSocio == true){
			Socio *socio = new Socio();

			socio->atualizarDados(nome,email,numero,cpf,categoria.size(),categoria);

			return socio;
		}
	}
	else{
		ClearScreen();
		cout << "Cadastro nao encontrado!\nPressione enter para continuar..." << endl;
		getchar();
		Cliente *cliente = NULL;
		int resp;

		while(1)
		{	
			ClearScreen();
			cout << "Deseja criar cadastro? 1. para confirmar\t\t2. para recusar" << endl;

			cin >> resp;

			if(resp > 2 || resp < 1)
				cout << "Escolha invalida. Digite novamente:" << endl;
			else
				break;
		}
		ClearScreen();
		if(resp == 1)
		{

			cout << "Digite o email:" << endl;
			cin >> email;
			clienteAux.setEmail(email);

			cout << "Digite o numero de celular com o DDD:" << endl; 
			cin >> numero; 
			clienteAux.setNumeroCel(numero);

			cout << "Digite o CPF (somente numeros): " << endl;
			cin >> cpf;
			clienteAux.setCPF(cpf); 
		
			ClearScreen();

			atualizarLista(&categoriaAux);

			while(1)
			{
				int respAux;
				int respAux1;
			
				cout << "Escolha categoria preferida: (digite " << categoriaAux.size() << " para pular) "<< endl;

				for(int i = 0;i < categoriaAux.size();i++)
				{
					cout << i << "." << categoriaAux[i] << endl;
				}

				cin >> respAux;

				if(respAux < categoriaAux.size()){
					clienteAux.setCategoria(categoriaAux[respAux]);
					categoria.push_back(categoriaAux[respAux]);
					cout << "Deseja adicionar mais alguma categoria preferida? \n1.Sim\t0.Nao\n" << endl;

					cin >> respAux1;

					if(respAux1 == 0)
						break;
				}				
				else if(resp == categoriaAux.size())
					break;
				else
					cout << "Opcao invalida. Digite novamente!" << endl;	
			}		
		}

		while(resp == 1){
			int resp2;
			ClearScreen();

			cout << "Cliente e socio? 1.sim\t\t2.nao" << endl;
			cin >> resp2;


			if(resp2 == 1){
				cliente = new Socio();
				cliente->atualizarDados(nome,email,numero,cpf,categoria.size(),categoria);
				cliente->cadastrar();
				break;
			}
			else if(resp2 == 2){
				cliente = new Cliente();
				cliente->atualizarDados(nome,email,numero,cpf,categoria.size(),categoria);
				cliente->cadastrar();
				break;
			}
			else
				cout << "Escolha invalida. Escolha novamente" << endl;
		}

		return cliente;
	}

	return NULL;	
}

bool encontrarCliente(Cliente *cliente)
{
	string nome;
	string email;
	string numero;
	string cpf;
	int numeroCat;
	vector<string> categoria;
	string categoriaAux;

	ifstream ClientFile("clientes.txt", ios::in);
	
	if (!ClientFile) 
	{	
		return false;
	}	

	getline(ClientFile,nome);
	getline(ClientFile,email);
	getline(ClientFile,numero);
	getline(ClientFile,cpf);

	ClientFile >> numeroCat;
	getline(ClientFile,categoriaAux);
	for(int i = 0;i < numeroCat; i++){
		getline(ClientFile,categoriaAux);
		categoria.push_back(categoriaAux);
	}

	if(nome.compare(cliente->getNome()) == 0){

			ClientFile.close();

			cliente->setNome(nome);
			cliente->setEmail(nome);
			cliente->setNumeroCel(nome);
			cliente->setCPF(nome);

			for(int i = 0;i < numeroCat;i++){
				cliente->setCategoria(categoria[i]);
			}

			return true;
	}

	ClientFile.close();	

	return false;
}

bool encontrarSocio(Cliente *cliente)
{
	string nome;
	string email;
	string numero;
	string cpf;
	int numeroCat;
	vector<string> categoria;
	string categoriaAux;

	ifstream ClientFile("socios.txt", ios::in);
	
	if (!ClientFile) 
	{	
		return false;
	}	

	getline(ClientFile,nome);
	getline(ClientFile,email);
	getline(ClientFile,numero);
	getline(ClientFile,cpf);

	ClientFile >> numeroCat;

	getline(ClientFile,categoriaAux);
	for(int i = 0;i < numeroCat; i++){
		getline(ClientFile,categoriaAux);
		categoria.push_back(categoriaAux);
	}

	if(nome.compare(cliente->getNome()) == 0){

			ClientFile.close();

			cliente->setNome(nome);
			cliente->setEmail(nome);
			cliente->setNumeroCel(nome);
			cliente->setCPF(nome);
			for(int i = 0;i < numeroCat;i++){
				cliente->setCategoria(categoria[i]);
			}
			
			return true;
	}

	ClientFile.close();		
			
	return false;
}
