#include "estoque.hpp"
#include "produto.hpp"
#include "carrinho.hpp"
#include <vector>
#include <string>


Carrinho::Carrinho()
{

}

Carrinho::~Carrinho()
{

}

double Carrinho::calcularTotal()
{	
	double sum = 0.0;

	for(int i = 0;i < getSizeProduto();i++)
	{
		sum += (getProduto(i))->getValor() * getQuantidade(i);
	}

	return sum;
}
