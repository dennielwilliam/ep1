#include "socio.hpp"
#include <iostream>
#include <fstream>


Socio::Socio()
{	
	setNome(" ");
	setEmail(" ");
	setNumeroCel(" ");
	setCPF(" ");
}

Socio::~Socio()
{	
	
}

double Socio::pagamentoTotal(Carrinho *carrinho)
{
	return carrinho->calcularTotal()*0.85;
}

void Socio::cadastrar()
{	

	ofstream cadastroFile("socios.txt", ios::app);
	
	if (!cadastroFile) 
	{
		cerr << "Nao foi possivel gravar no arquivo" << endl;
		exit(1);
	}

	cadastroFile << getNome();
	cadastroFile.put('\n');

	cadastroFile << getEmail();
	cadastroFile.put('\n');

	cadastroFile << getNumeroCel();
	cadastroFile.put('\n');

	cadastroFile << getCPF();
	cadastroFile.put('\n');

	cadastroFile << getSizeCategoria();
	cadastroFile.put('\n');

	for(int i = 0;i < getSizeCategoria();i++){
		cadastroFile << getCategoria(i);
		cadastroFile.put('\n');
	}

	cadastroFile.close();

} 