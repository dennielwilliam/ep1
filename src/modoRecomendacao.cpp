#include "cliente.hpp"
#include "carrinho.hpp"
#include "estoque.hpp"
#include "produto.hpp"
#include "modoVenda.hpp"
#include "modoEstoque.hpp"
#include "modoRecomendacao.hpp"
#include "aux.hpp"
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>

void modoRecomendacao()
{
	Cliente cliente;
	string nome;
	bool resultSocio;
	bool resultCliente;

	while(1){
		ClearScreen();
		getchar();
		cout << "Digite o nome: (somente o primeiro nome)" << endl;
		getline(cin, nome);
		cliente.setNome(nome);

		resultCliente = encontrarCliente(&cliente);

		if(resultCliente == false)
			resultSocio = encontrarSocio(&cliente);

		if(resultCliente == true ||	resultSocio == true)
		{	
			ClearScreen();
			cout << "Cadastro Encontrado! Pressione enter para continuar" << endl;
			getchar();
			produtosRecomendados(&cliente);
			return;
		}
		else
		{	
			int resp;
			ClearScreen();
			cout << "Cadastro nao encontrado!\nPor favor selecione o modo venda para fazer o cadastro." << endl;
			cout << "\nDeseja continuar no modo recomendacao? 1.Sim\t\t\t2.Nao" << endl;
			cin >> resp;
			if(resp == 2)
				return;
		}
	}
}

void produtosRecomendados(Cliente *cliente)
{
	Estoque estoque;
	vector <string> categoria;

	atualizarEstoque(&estoque);

	for(int i = 0;i < cliente->getSizeCategoria();i++)
		categoria.push_back(cliente->getCategoria(i));

	cout << "Categorias Recomendadas: " << endl;

	for(int i = 0;i < categoria.size() && i < 10;i++)
		cout << "--" << categoria[i] << "--" << endl;
	cout << "Pressione enter para continuar..." << endl;
	getchar();
}

