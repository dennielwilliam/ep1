#include "cliente.hpp"
#include "carrinho.hpp"
#include "estoque.hpp"
#include "produto.hpp"
#include "modoVenda.hpp"
#include "modoEstoque.hpp"
#include "modoRecomendacao.hpp"
#include "aux.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;


int main()
{	
	int n;

	while(n)
	{	
		ClearScreen();
		cout << "Selecione o modo de operacao: " << endl;
		cout << "1. Modo venda" << endl;
		cout << "2. Modo estoque" << endl;
		cout << "3. Modo recomendacao" << endl;
		cout << "0. Fechar o programa" << endl;
		cin >> n;

		ClearScreen();

		if(n == 1)
			modoVenda();
		else if(n == 2)
			modoEstoque();
		else if(n == 3)
			modoRecomendacao();

		ClearScreen();

		if(n < 0 || n > 3)
			cout << "Opcao inexistente. Escolha novamente" << endl;
	}

	return 0;
}



