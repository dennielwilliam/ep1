#include "produto.hpp"
#include <iostream>

using namespace std;

Produto::Produto()
{
	nome = " ";
	valor = 0.0;
}

Produto::~Produto()
{

}

void Produto::setNome(string nome)
{
	this->nome = nome;
}

string Produto::getNome()
{
	return nome;
}

void Produto::setCategoria(string categoria)
{
	this->categoria.push_back(categoria);
}

string Produto::getCategoria(int i)
{
	return categoria[i];
}

unsigned Produto::getSizeCategoria()
{
	return categoria.size();
}

void Produto::setValor(double valor)
{
	this->valor = valor;
}

double Produto::getValor()
{
	return valor;
}